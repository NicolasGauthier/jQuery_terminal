package fr.imie.lightapi.restservice.model;

import fr.imie.lightapi.restservice.exception.DirectoryInexistingException;
import fr.imie.lightapi.restservice.exception.DirectoryNotEmptyException;
import fr.imie.lightapi.restservice.exception.IsRootDirectoryException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Classe Directory, contenant les information relatives par rapport
 * au dossier dans le lequel l'utilisateur se trouve, le dossier racine, ainsi
 * que le sepaateur utilisé selon l'OS de l'utilisateur.
 *
 * Cette classe contient également les méthodes permettant de simuler les commandes
 * suivantes :
 * rmdir
 * mkdir
 * cd
 * ~
 * ls
 * cat
 */
public class Directory {

    private String rootDirectory;
    private String currentDirectory;
    private char separator;

    public Directory(char separator) {
        this.setRootDirectory(System.getProperty("user.dir"));
        this.setCurrentDirectory(this.getRootDirectory());
        this.setSeparator(separator);
    }

    public String getFormatedDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return dateFormat.format(new Date());
    }

    /**
     * Retourne la version de Java utilisé
     * @return String : Version de java
     */
    public String getJavaVersion() {
        return System.getProperty("java.runtime.version");
    }

    /**
     * Methode retournant sous la forme de map les differentes informations
     * relative à l'utilisation de la mémoire disque
     *
     * @return Map<String, Long>
     */
    public Map<String, Long> getAvailableSpace() {
        File file = new File(this.getCurrentDirectory());
        Map<String, Long> results = new HashMap<>();
        results.put("TotalSpace", file.getTotalSpace() );
        results.put("UsableSpace", file.getUsableSpace() );
        results.put("AvailableSpace", results.get("TotalSpace") - results.get("UsableSpace") );
        return results;
    }

    /**
     * Methode simulant le rmdir du dossier dont le nom est donné en paramètre
     * @param directoryName : nom du fichier à supprimer
     * @return isDelete : si le dossier à put être supprimé
     */
    public boolean deleteDirectory(String directoryName) throws DirectoryNotEmptyException, DirectoryInexistingException {
        File file = new File(this.getCurrentDirectory() + this.getSeparator() + directoryName);
        boolean isDelete = true;
        /* Traitement des cas d'exclusion */
        if (! file.exists() || !file.isDirectory()) {
            throw new DirectoryInexistingException("Le dossier spécifié n'existe pas, ou pas à l'emplacement actuel");
        }
        try {
            isDelete = file.delete();
        } catch (Exception e) {
            isDelete = false;
        }
        return isDelete;
    }

    /**
     * Methode simulant le cd {filename} depuis le currentDirectory
     * @param name : nom du dossier
     * @return currentirectory : Path du dossier actuel
     * @throws DirectoryInexistingException : CustomException
     */
    public String changeDirectory(String name) throws DirectoryInexistingException {
        File directory = new File(this.getCurrentDirectory() + this.getSeparator() + name);
        if (!directory.exists() || !directory.isDirectory()) {
            throw new DirectoryInexistingException("Le Dossier spécifié n'existe pas ou est introuvage");
        } else {
            this.setCurrentDirectory(directory.getPath());
        }
        return this.getCurrentDirectory();
    }

    /**
     * Methode simulant le cd ..
     * @return
     * @throws IsRootDirectoryException
     */
    public String changePreviousDirectory() throws IsRootDirectoryException {
        String[] directoriesSplit = this.getCurrentDirectory().split(Pattern.quote("\\"));
        if ( directoriesSplit.length < 2 ) {
            throw new IsRootDirectoryException("Le dossier dans lequel vous êtes est la racine de votre disque");
        }
        StringBuffer newPath = new StringBuffer();
        for (int i = 0; i < (directoriesSplit.length - 1); i++ ) {
            if ( i != 0 ) {
                newPath.append(this.getSeparator());
            }
            newPath.append(directoriesSplit[i]);
        }
        this.setCurrentDirectory(newPath.toString());
        return newPath.toString();
    }

    /**
     * Methode simulant le cat d'un fichier dont le nom est donné
     * en paramètre et le chemin est le currentDirectory
     *
     * @param nameFile : nom du fichier
     * @return file : le fichier sous forme d'un string
     * @throws IOException
     */
    public String catFile(String nameFile) throws IOException {
        BufferedReader br = null;
        FileReader fr = null;
        StringBuffer file = new StringBuffer();
        String line;
        try {
            fr = new FileReader(this.getCurrentDirectory() + this.getSeparator() + nameFile);
            br = new BufferedReader(fr);
            while ((line = br.readLine()) != null) {
                file.append(line);
            }
        } catch (IOException e) {
            throw e;
        }
        return file.toString();
    }


    /**
     * Methode simulant un ls dans le currentDirectory
     *
     * @return
     * @throws Exception
     */
    public List<String> listSegments() throws Exception {
        List<String> ls = new ArrayList<>();
        try {
            File current = new File(this.getCurrentDirectory());
            Collections.addAll(ls, current.list());
        } catch (Exception e) {
            throw e;
        }
        return ls;
    }

    /**
     * Methode permettant de creer un dossier à l'endroit du currentDirectory
     *
     * @param name : Nom du dossier à creer
     * @return create : statut de la création du dossier
     */
    public boolean createDirectory(String name) {
        File f = null;
        boolean create;
        try {
            f = new File(this.getCurrentDirectory() + this.getSeparator() + name);
            f.mkdir();
            create = true;
        } catch (Exception e) {
            create = false;
        }
        return create;
    }

    /**
     * Methode simulant le retour au home ( root directory )
     *
     * @return currentDirectory
     */
    public String goHome() {
        this.setCurrentDirectory(this.getRootDirectory());
        return this.getCurrentDirectory();
    }

    public String getRootDirectory() {
        return rootDirectory;
    }

    private void setRootDirectory(String rootDirectory) {
        this.rootDirectory = rootDirectory;
    }

    public String getCurrentDirectory() {
        return currentDirectory;
    }

    private void setCurrentDirectory(String currentDirectory) {
        this.currentDirectory = currentDirectory;
    }

    public char getSeparator() {
        return separator;
    }

    public void setSeparator(char separator) {
        this.separator = separator;
    }
}
