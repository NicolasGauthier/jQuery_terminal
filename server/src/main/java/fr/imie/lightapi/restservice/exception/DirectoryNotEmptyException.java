package fr.imie.lightapi.restservice.exception;

public class DirectoryNotEmptyException extends Exception {

    private String message;

    public DirectoryNotEmptyException() {
        super();
    }

    public DirectoryNotEmptyException(String message) {
        super(message);
    }
}
