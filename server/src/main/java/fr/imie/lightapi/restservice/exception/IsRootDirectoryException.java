package fr.imie.lightapi.restservice.exception;

public class IsRootDirectoryException extends Exception {
    public IsRootDirectoryException() {
    }

    public IsRootDirectoryException(String message) {
        super(message);
    }
}
