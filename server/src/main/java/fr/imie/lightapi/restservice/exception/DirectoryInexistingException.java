package fr.imie.lightapi.restservice.exception;

public class DirectoryInexistingException extends Exception {

    private String message;

    public DirectoryInexistingException() {
        super();
    }

    public DirectoryInexistingException(String message) {
        super(message);
    }
}
