/**
 * Fonction appelée lorsque l'on clique sur Enter
 */
function commandLineEnter() {
    addElementToTerminalWindow("<p>" + $("#current-line").text() + $("#input-term").val() + "</p>");
    analyzeInput();
}

function initHistoric() {
    if (localStorage.getItem("historicCmd") === undefined || localStorage.getItem("historicCmd") === null) {
        var arr = [];
        localStorage.setItem("historicCmd", JSON.stringify(arr));
    }
}

function resetHistoric() {
    var arr = [];
    localStorage.setItem("historicCmd", JSON.stringify(arr));
    initHistoric();
}

function growHistIndex() {
    if (getHistoricSize() > histIndex) {
        histIndex++;
    }
}

function reduceHistIndex() {
    if (histIndex > -1) {
        histIndex--;
    }
}

function getItemInHistory() {
    var hist = localStorage.getItem("historicCmd");
    hist = JSON.parse(hist);
    $("#input-term").val(hist[getHistoricSize() - 1 - histIndex])
}

function getHistoricSize() {
    var hist = localStorage.getItem("historicCmd");
    hist = JSON.parse(hist);
    return hist.length;
}

function saveCommandEntered(cmd) {
    var hist = localStorage.getItem("historicCmd");
    hist = JSON.parse(hist);
    hist.push(cmd);
    localStorage.setItem("historicCmd", JSON.stringify(hist));
}

function displayHistoricCommand(size, nb) {
    var hist = localStorage.getItem("historicCmd");
    hist = JSON.parse(hist);
    size--;
    var i = 0;
    var p = document.createElement("p");
    while (i < nb) {
        $(p).prepend("<span>" + hist[size - i] + "</span><br>");
        i++;
    }
    addElementToTerminalWindow(p);
}

/**
 * Methode permettant l'affichage des dernières commandes saisies (20 par defaut)
 * @param optional
 */
function getLocalHistoricCommand(optional) {
    optional = optional || 20;
    var size = getHistoricSize();
    (size < optional) ? displayHistoricCommand(size, size) : displayHistoricCommand(size, optional);
}


function autoScrollBottom() {
    $("#terminal-window").scrollTop($("#terminal-window").height() * 1000);
}

function clearTerminal() {
    $("#previous-line").empty();
}

function precisionRound(number, precision) {
    var factor = Math.pow(10, precision);
    return Math.round(number * factor) / factor;
}

/**
 * Vide l'input du terminal
 */
function clearInputTerminal() {
    $("#input-term").val("");
}

/**
 * Ajoute l'element en paramètre dans les ligne passées du terminal
 * @param element
 */
function addElementToTerminalWindow(element) {
    $("#previous-line").append(element);
}

function commandNotFound(commande) {
    addElementToTerminalWindow("<p>La commande \"" + commande + "\" que vous avez saisi n'existe pas." +
        " Pour plus d'informations sur les commandes disponibles, veuillez saisir la commande " +
        "\"man\" ou \"?\"</p>")
}

function selectTerminal() {
    $("#input-term").focus();
}

function evaluate(inputString) {
    inputString = inputString.replace("=", "").trim();
    try {
        addElementToTerminalWindow("<p>" + eval(inputString) + "</p>");
    } catch (error) {
        addElementToTerminalWindow("<p>" + error + "</p>");
    }
}

function analyzeInput() {
    if ($("#input-term").val().trim().charAt(0) === "=") {
        addElementToTerminalWindow(evaluate($("#input-term").val()))
    } else {
        var inputSplited = $("#input-term").val().toLowerCase().trim().split(" ");
        useInputSplitted(inputSplited);
    }
}

function exitTerminal() {
    var logout = "<p>Fin de votre session. Veuillez clicker sur F5 pour en ouvrir une nouvelle</p>";
    $("#terminal-window").empty().append(logout);
}

function switchTheme() {
    $("#input-term").toggleClass("darktheme");
    $("#input-term").toggleClass("whitetheme");
    $("#terminal-window").toggleClass("darktheme");
    $("#terminal-window").toggleClass("whitetheme");
    $("#title").toggleClass("darktheme-title");
    $("#title").toggleClass("whitetheme-title");
    $("body").toggleClass("darktheme-background");
    $("body").toggleClass("whitetheme-background");
    if ($("#switch-theme").text() !== "Switch to Light Theme") {
        $("#switch-theme").text("Switch to Light Theme");
    } else {
        $("#switch-theme").text("Switch to Dark Theme");
    }
}

function detectOs() {
    var separator = "\\";
    if (navigator.appVersion.indexOf("Win") != -1) OSName = "\\";
    if (navigator.appVersion.indexOf("Mac") != -1) OSName = "/";
    if (navigator.appVersion.indexOf("X11") != -1) OSName = "/";
    if (navigator.appVersion.indexOf("Linux") != -1) OSName = "/";
    sendPathSeparatorOs(separator);
}

function displayDoc() {
    var docDiv = document.createElement("div");
    $(docDiv).append("<p>Liste des commandes disponibles :</p>");
    $(docDiv).append("<p>&emsp;=&emsp;&emsp;Effectue un calcul </p>");
    $(docDiv).append("<p>&emsp;clear&emsp;&emsp;Vide le terminal</p>");
    $(docDiv).append("<p>&emsp;exit&emsp;&emsp; Quitter le terminal</p>");
    $(docDiv).append("<p>&emsp;date&emsp;&emsp; Retourne la date du jour, dans le format français</p>");
    $(docDiv).append("<p>&emsp;java -v&emsp;&emsp;Retourne la version java utilisé pour le serveur</p>");
    $(docDiv).append("<p>&emsp;free&emsp;&emsp;Retourne les informations concernant l'espace disque utilisé</p>");
    $(docDiv).append("<p>&emsp;cd {dir}&emsp;&emsp;Permet de se deplacer dans le dossier indiqué</p>");
    $(docDiv).append("<p>&emsp;cd ..&emsp;&emsp;Permet de se deplacer dans le dossier précédent, si possible</p>");
    $(docDiv).append("<p>&emsp;~&emsp;&emsp;Permet de revenir à votre home ( l'endoit ou se trouve le jar)</p>");
    $(docDiv).append("<p>&emsp;ls&emsp;&emsp;Liste les fichiers et dossier dans le dossier actuel</p>");
    $(docDiv).append("<p>&emsp;mkdir {dir}&emsp;&emsp;Creer un dossier du nom indiqué à l'emplacement actuel</p>");
    $(docDiv).append("<p>&emsp;rmkdir {dir}&emsp;&emsp;Creer un dossier du nom indiqué à l'emplacement actuel</p>");
    $(docDiv).append("<p>&emsp;cat {file}&emsp;&emsp;Permet de lire le fichier dans le terminal</p>");
    $(docDiv).append("<p>&emsp;history -{nb}&emsp;&emsp;Permet de voir les nb dernière commandes ( 20 si non spécifié )</p>");
    $(docDiv).append("<p>&emsp;reset&emsp;&emsp;Supprime l'historique des commandes</p>");
    $(docDiv).append("<p>&emsp;down / up&emsp;&emsp;Naviguer parmis les commandes précédentes</p>");
    $(docDiv).append("<p>&emsp;man ou ?&emsp;&emsp;Affiche les differentes commandes disponibles</p>");
    addElementToTerminalWindow(docDiv);
}

function useInputSplitted(inputSplited) {
    switch (String(inputSplited[0])) {
        case "clear":
            clearTerminal();
            break;
        case "exit":
            exitTerminal();
            break;
        case "date":
            getCurrentDate();
            break;
        case "free":
            getFreeSpace();
            break;
        case "ls":
            listSegments();
            break;
        case "cat":
            catFile(inputSplited[1]);
            break;
        case "mkdir":
            makeDirectory(inputSplited[1]);
            break;
        case "rmdir":
            removeDirectory(inputSplited[1]);
            break;
        case "history":
            getLocalHistoricCommand();
            break;
        case "reset":
            resetHistoric();
            clearTerminal();
            break;
        case "cd":
            if (inputSplited[1] === "..") {
                changeUnderDirectory();
            } else {
                changeDirectory(inputSplited[1]);
            }
            break;
        case "~":
            goHome();
            break;
        case "man":
        case "?":
            displayDoc();
            break;
        case "java":
            if (inputSplited[1] === "-v") {
                getJavaVersion();
            }
            break;
        default:
            commandNotFound(inputSplited[0]);
    }
}

