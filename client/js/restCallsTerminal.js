const localhost = "http://127.0.01:8080/";

/**
 * CURRENT DIRECTORY
 */
function getCurrentDirectory() {
    $.ajax({
        url: localhost + 'api/directory/current',
        type: 'GET',
        dataType: 'text',
        success: function (data, statut) {
            $("#path").text(data);

        },
        error: function (data, statut, erreur) {
            alert("Erreur = " + statut);
        },
        complete: function (data, statut) {
        }
    });
}

function getCurrentDate() {
    $.ajax({
        url: localhost + 'api/directory/date',
        type: 'GET',
        dataType: 'text',
        success: function (data, statut) {
            addElementToTerminalWindow("<p>" + data + "</p>")

        },
        error: function (data, statut, erreur) {
            alert("Une erreur du serveur empêche la recupération du statut = " + data);
        },
        complete: function (data, statut) {
        }
    });
}

function changeUnderDirectory() {
    $.ajax({
        url: localhost + 'api/directory/cd/under',
        type: 'POST',
        dataType: 'text',
        success: function (data, statut) {
            $("#path").text(data);
        },
        error: function (data, statut, erreur) {
            data = JSON.parse(data);
            addElementToTerminalWindow("<p>" + data + "</p>");
        },
        complete: function (data, statut) {
        }
    });
}

function changeDirectory(dirname) {
    $.ajax({
        url: localhost + 'api/directory/cd/' + dirname,
        type: 'POST',
        dataType: 'text',
        success: function (data, statut) {
            $("#path").text(data);
        },
        error: function (data, statut, erreur) {
            addElementToTerminalWindow("<p>Le dossier spécifié n'existe pas. Pour consulter les disponibles à votre position, veuillez taper la commande 'ls'</p>");
        },
        complete: function (data, statut) {
        }
    });
}

function catFile(filename) {
    $.ajax({
        url: localhost + 'api/directory/cat',
        type: 'POST',
        dataType: 'text',
        data: {
            'filename': filename
        },
        success: function (data, statut) {
            addElementToTerminalWindow("<pre>" + data + "</pre>")
        },
        error: function (data, statut, erreur) {
            addElementToTerminalWindow("<p>data</p>");
        },
        complete: function (data, statut) {
        }
    });
}

function makeDirectory(dirname) {
    $.ajax({
        url: localhost + 'api/directory/mkdir/' + dirname,
        type: 'POST',
        dataType: 'text',
        success: function (data, statut) {
        },
        error: function (data, statut, erreur) {
            addElementToTerminalWindow("<p>data</p>");
        },
        complete: function (data, statut) {
        }
    });
}

function removeDirectory(dirname) {
    $.ajax({
        url: localhost + 'api/directory/rmdir/' + dirname,
        type: 'DELETE',
        dataType: 'text',
        success: function (data, statut) {
        },
        error: function (data, statut, erreur) {
        },
        complete: function (data, statut) {
        }
    });
}

function listSegments() {
    $.ajax({
        url: localhost + 'api/directory/ls',
        type: 'POST',
        dataType: 'text',
        success: function (data, statut) {
            data = JSON.parse(data);
            var segments = document.createElement("p");
            data.forEach(function (elem) {
                $(segments).append("<span>" + elem + "&nbsp;&nbsp;</span>");
            })
            addElementToTerminalWindow(segments);
        },
        error: function (data, statut, erreur) {
            alert("Une erreur du serveur empêche la recupupération des données. " + data);
        },
        complete: function (data, statut) {
        }
    });
}

function getJavaVersion() {
    $.ajax({
        url: localhost + 'api/directory/version',
        type: 'GET',
        dataType: 'text',
        success: function (data, statut) {
            addElementToTerminalWindow("<p>" + data + "</p>")

        },
        error: function (data, statut, erreur) {
            alert("Une erreur du serveur empêche la recupupération des données. " + data);
        },
        complete: function (data, statut) {
        }
    });
}

function goHome() {
    $.ajax({
        url: localhost + 'api/directory/gohome',
        type: 'GET',
        dataType: 'text',
        success: function (data, statut) {
            $("#path").text(data);
        },
        error: function (data, statut, erreur) {
            alert("Une erreur serveur est survenu", erreur, statut);
        },
        complete: function (data, statut) {
        }
    });
}

function getFreeSpace() {
    $.ajax({
        url: localhost + 'api/directory/free',
        type: 'GET',
        dataType: 'text',
        success: function (data, statut) {
            data = JSON.parse(data);
            addElementToTerminalWindow("<p>Espace Utilisé : " + precisionRound(parseFloat((data.UsableSpace) / 1073741824), 2) + " Go</p>")
            addElementToTerminalWindow("<p>Espace Libre : " + precisionRound(parseFloat((data.AvailableSpace) / 1073741824), 2) + " Go</p>")
            addElementToTerminalWindow("<p>Espace Total : " + precisionRound(parseFloat((data.TotalSpace) / 1073741824), 2) + " Go</p>")

        },
        error: function (data, statut, erreur) {
            alert("Une erreur du serveur empêche la recupupération des données. " + data);
        },
        complete: function (data, statut) {
        }
    });
}


function sendPathSeparatorOs(separator) {
    $.ajax({
        url: localhost + 'api/directory/os',
        type: 'POST',
        dataType: 'text',
        data: {
            'separator': separator
        },
        success: function (data, statut) {
        },
        error: function (data, statut, erreur) {
            alert("Impossible de detecter votre OS utilisé. Le separateur de Path sera celui de Windows (\"\\\")");
        },
        complete: function (data, statut) {
        }
    });
}

